/*
ICFCallServer runs on imagent, which lacks access to AddressBook.
Otherwise, we could have hooked -[ICFCallServer _requestCallGrantForIdentifier:forService:waitForResponse:completionBlock:].

Fortunately, IncomingCallFilter.framework provides a method to register call filters from peers.
To use this, we need to inject a different process that meets the following requirements:
- launchd.plist["KeepAlive"]: to stay registered with ICFCallServer
- entitlements["com.apple.private.icfcallserver"]: to communicate with ICFCallServer
- entitlements["com.apple.private.tcc.allow"].contains("kTCCServiceAddressBook"): to use AddressBook API

On iOS 8.4, these are met by vmd and SpringBoard.
The logical choice, callservicesd, does not have KeepAlive set.
*/

#import <AddressBook/AddressBook.h>
#import <CoreFoundation/CoreFoundation.h>

extern void ICFRegisterCallFilterBlockWithIdentifier(Boolean(^)(CFStringRef address,CFStringRef service),CFStringRef identifier);
extern CFStringRef ICFServiceIdentifierFaceTime;
extern CFStringRef ICFServiceIdentifierFaceTimeAudio;
extern CFStringRef ICFServiceIdentifierTelephony;
extern CFStringRef ICFUnknownID;

extern ABRecordRef ABAddressBookFindPersonMatchingPhoneNumber(ABAddressBookRef book,CFStringRef phone,int* index,CFStringRef* label);

static const CFStringRef appID=CFSTR("com.officialscheduler.callblocker");
static __attribute__((constructor)) void init() {
  ICFRegisterCallFilterBlockWithIdentifier(^Boolean(CFStringRef address,CFStringRef service){
    if(CFEqual(service,ICFServiceIdentifierTelephony) && CFPreferencesGetAppBooleanValue(CFSTR("enabled"),appID,NULL)){
      if(CFEqual(address,ICFUnknownID)){return !CFPreferencesGetAppBooleanValue(CFSTR("blockUnknown"),appID,NULL);}
      ABAddressBookRef book=ABAddressBookCreateWithOptions(NULL,NULL);
      if(book && ABAddressBookGetPersonCount(book)){
        Boolean found=ABAddressBookFindPersonMatchingPhoneNumber(book,address,NULL,NULL)?true:false;
        CFRelease(book);
        return found;
      }
    }
    return true;
  },appID);
}
