ARCHS = arm64

include theos/makefiles/common.mk

TWEAK_NAME = CallBlocker
CallBlocker_FILES = Tweak.c
CallBlocker_FRAMEWORKS = AddressBook CoreFoundation
CallBlocker_PRIVATE_FRAMEWORKS = IncomingCallFilter

include $(THEOS_MAKE_PATH)/tweak.mk
